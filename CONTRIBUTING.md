# Development guide

## Setting up a local environment

```bash
conda env create -f environment.yml
conda activate cirun-dev
pip install -e .
pip install -e git+https://gitlab.cern.ch/lhcb-gitlab-runners/cirun_plugin_example.git
```

## Running locally using docker swarm

```bash
docker stack deploy -c docker-compose.yml cirun
docker service ls
docker service logs -f cirun_webapp
docker service logs -f cirun_celery-worker
docker stack rm cirun
```
