#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

# Install on launch to allow bind mounts to be used during development
pip install --upgrade git+https://gitlab.cern.ch/lhcb-gitlab-runners/runner-api.git --no-deps -vv
pip install /source --no-deps -vv
if [ -n "${CIRUN_EXTRA_REQUIREMENTS_FN:-}" ]; then
    pip install -r "${CIRUN_EXTRA_REQUIREMENTS_FN}"
fi

# Enable coverage if requested
if [ -n "${COVERAGE_PROCESS_START:-}" ]; then
    echo "Enabling coverage"
    cp /source/docker/base/enable_coverage.pth "$(python -c "import site; print(site.getsitepackages()[0])")/enable_coverage.pth"
else
    echo "Not enabling coverage"
fi

exec "$@"
