from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import json

from celery import Celery
from gitlab_runner_api import Runner as GitLabRunner, Job as GitLabJob, failure_reasons as gitlab_failure_reasons

from cirun.logging import log
from cirun.utils import available_plugins, Runner
from . import config as celery_config

app = Celery('cirun.celery', config_source=celery_config)


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    try:
        log.info(Runner.read_all())
        for runner in Runner.read_all().values():
            if not runner.active:
                log.info('%s (%s) is marked inactive, skipping', runner.name, runner.plugin_name)
                continue
            if runner.plugin_name in available_plugins:
                # Check for new pipelines every JOB_REQUEST_WAIT seconds
                job_request_wait = getattr(runner.plugin, 'JOB_REQUEST_WAIT', 5)
                log.info('Registering periodic task for "%s" using %s (period=%ss)',
                         runner.name, runner.plugin_name, job_request_wait)
                runner_data = runner.gitlab_runner.dumps()
                sender.add_periodic_task(job_request_wait, pull_job.s(runner.plugin_name, runner.name, runner_data),
                                         expires=job_request_wait, name='pull-job-'+runner.name)
            else:
                log.error('Failed to find plugin %s for "%s", skipping...', runner.plugin_name, runner.name)
    except Exception:
        log.exception('Failed to run setup_periodic_tasks')


@app.task
def pull_job(plugin_name, name, runner_data):
    runner = GitLabRunner.loads(runner_data)
    log.debug('Polling for job for "%s" using %s', name, runner)
    plugin = available_plugins[plugin_name]

    while True:
        job = runner.request_job()
        if job is None:
            break

        log.debug('Starting %s using %s', job, plugin_name)
        try:
            still_running, state = plugin.start_job(job)
        except Exception:
            kill_job(job, None, plugin_name, 'Failed to start {job} using {plugin_name}')
        else:
            if still_running:
                schedule_monitoring(plugin_name, name, job, state)
            else:
                log.debug('Job %s for "%s" using %s terminated immediately', job, name, plugin_name)

        if getattr(plugin, 'GET_MULTIPLE_JOBS_PER_CYCLE', False):
            log.debug('GET_MULTIPLE_JOBS_PER_CYCLE is enabled, re-requesting')
            continue

        break


@app.task
def monitor_job(plugin_name, name, job_data, state_data):
    job = GitLabJob.loads(job_data)
    log.debug('Monitoring %s using %s', job, plugin_name)
    plugin = available_plugins[plugin_name]
    state = json.loads(state_data)

    try:
        job.auth()
        still_running, state = plugin.monitor_job(job, state)
    except Exception:
        kill_job(job, state, plugin_name, 'Failed to start {job} using {plugin_name}')
    else:
        if still_running:
            schedule_monitoring(plugin_name, name, job, state)
        else:
            log.debug('Job %s for "%s" using %s completed', job, name, plugin_name)


def schedule_monitoring(plugin_name, name, job, state):
    try:
        state_data = json.dumps(state)
    except TypeError:
        kill_job(job, state, plugin_name, 'ERROR: Received invalid state from {plugin_name} for {job}')
        return

    job_monitor_wait = getattr(available_plugins[plugin_name], 'JOB_MONITOR_WAIT', 30)
    monitor_job.s(plugin_name, name, job.dumps(), state_data).apply_async(countdown=job_monitor_wait)


def kill_job(job, state, plugin_name, message):
    log.exception(message.format(job=job, plugin_name=plugin_name))

    try:
        job.log += 'ERROR: %s\n' % message.format(job='job', plugin_name=plugin_name)
        job.set_failed(gitlab_failure_reasons.RunnerSystemFailure())
    except Exception:
        log.exception('Failed to set job status as failed for %s', job)

    # Try to clean up after ourselves
    if state is not None:
        try:
            plugin = available_plugins[plugin_name]
            plugin.kill_job(state)
        except Exception:
            log.exception('Failed to kill job %s', job)
