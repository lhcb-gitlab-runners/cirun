import os
from os.path import expandvars
# celery_broker = os.environ['CIRUN_CELERY_BROKER']

# Broker settings.
broker_url = expandvars(os.environ['CIRUN_CELERY_BROKER'])
broker_use_ssl = os.environ.get('CIRUN_CELERY_USE_SSL', False)
if 'CIRUN_CELERY_LOGIN_METHOD' in os.environ:
    broker_login_method = os.environ['CIRUN_CELERY_LOGIN_METHOD']

# # List of modules to import when the Celery worker starts.
# imports = ('cirun.celery',)

del os
