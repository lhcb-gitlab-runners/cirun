from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import re
import uuid

from flask import Blueprint, redirect, url_for, request, render_template, flash, abort
from flask_login import current_user, login_required

from ..logging import log
from ..utils import Runner, available_plugins

bp = Blueprint('cirun', __name__)


@bp.route('/')
@login_required
def index():
    return render_template(
        'list_runners.html.j2',
        user=current_user,
        runners=Runner.read_all().values(),
        available_plugins=available_plugins
    )


@bp.route('/register_runner', methods=['POST'])
@login_required
def register_runner():
    error, (runner_name, url, plugin_name, tags, active) = _parse_runner_form()
    token = request.form.get('token', '')
    protected_only = bool(request.form.get('protected_only', False))
    if len(token) < 5 or len(token) > 200:
        flash('Token is invalid', 'error')
        error = True
    if protected_only:
        access_level = 'ref_protected'
    else:
        access_level = 'not_protected'

    if not error:
        import gitlab_runner_api
        gitlab_runner = gitlab_runner_api.Runner.register(
            'https://gitlab.cern.ch', token,
            description=runner_name, active=True, locked=True, tags=tags,
            run_untagged=False, access_level=access_level
        )
        internal_id = str(uuid.uuid4())
        runner = Runner(internal_id, gitlab_runner, runner_name, url, tags, plugin_name, active)
        log.info('Registered runner with internal ID %s and GitLab ID #%s', runner.id, runner.gitlab_runner.id)
        runner.write()

        flash('Runner registered successfully with GitLab ID #%s' % runner.gitlab_runner.id)

    return redirect(url_for('index'))


@bp.route('/edit/<id>')
@login_required
def edit_runner(id):
    runners = Runner.read_all()
    try:
        runner = runners[id]
    except KeyError:
        raise abort(404, 'Runner not found for %s' % id)

    return render_template(
        'edit_runner.html.j2',
        user=current_user,
        runner=runner,
        available_plugins=available_plugins
    )


@bp.route('/edit/<id>/save', methods=['POST'])
@login_required
def save_runner(id):
    error, (runner_name, url, plugin_name, tags, active) = _parse_runner_form()
    if error:
        return redirect(url_for('cirun.save_runner', id=id))

    runners = Runner.read_all()
    try:
        runner = runners[id]
    except KeyError:
        raise abort(404, 'Runner not found for %s' % id)
    runner.name = runner_name
    runner.url = url
    runner.plugin_name = plugin_name
    runner.tags = tags
    runner.active = active
    runner.write()
    flash('Successfully updated runner with GitLab ID #%s' % runner.gitlab_runner.id)

    return redirect(url_for('index'))


def _parse_runner_form():
    runner_name = request.form.get('name', '')
    url = request.form.get('url', '')
    plugin_name = request.form.get('plugin', '')
    raw_tags = request.form.get('tags', '')
    active = bool(request.form.get('active', False))

    error = False
    pattern = r'^[A-Za-z0-9-_ ]{5,50}$'
    if not re.match(pattern, runner_name):
        flash('Runner name must match <code>%s</code>' % pattern, 'error')
        error = True
    if not url.startswith('https://') or len(url) < 20 or len(url) > 1000:
        flash('URL must be valid and start with <code>https://</code>', 'error')
        error = True
    if plugin_name not in available_plugins:
        flash('Plugin <code>%s</code> is unknown' % plugin_name)
        error = True
    tags = []
    for tag in raw_tags.split(','):
        tag = tag.strip()
        if not tag:
            continue
        pattern = r'^[A-Za-z0-9-_]{5,10}$'
        if not re.match(pattern, tag):
            flash('Tags must each match <code>%s</code>' % pattern, 'error')
            error = True
        tags += [tag]

    return error, (runner_name, url, plugin_name, tags, active)
