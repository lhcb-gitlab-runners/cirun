from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
from os.path import dirname, join

from flask import Flask
from jinja2 import StrictUndefined


def create_app(test_config=None):
    """Create and configure an instance of the Flask application."""
    app = Flask('cirun', instance_relative_config=True,
                static_folder=join(dirname(__file__), '../data/static'),
                template_folder=join(dirname(__file__), '../data/template'))
    app.jinja_env.undefined = StrictUndefined
    app.config.from_envvar('CIRUN_SETTINGS')
    if not app.config.get('SECRET_KEY'):
        raise RuntimeError('SECRET_KEY must be set')

    if test_config is not None:
        app.config.update(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # if not app.debug:
    from .login import init_login
    init_login(app)

    # apply the blueprints to the app
    from . import routes
    app.register_blueprint(routes.bp)

    # make url_for('index') == url_for('blog.index')
    # in another app, you might define a separate main index here with
    # app.route, while giving the blog blueprint a url_prefix, but for
    # the tutorial the blog will be the main index
    app.add_url_rule('/', endpoint='index')

    return app
