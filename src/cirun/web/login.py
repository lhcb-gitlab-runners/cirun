from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import timedelta
import os

from authlib.common.errors import AuthlibBaseError
from authlib.flask.client import OAuth
from flask import abort, url_for, redirect
from flask_login import LoginManager, UserMixin, login_user, logout_user

from ..logging import log


class User(UserMixin):
    def __init__(self, username):
        self._username = username

    def __repr__(self):
        return 'User(%r)' % self._username

    def get_id(self):
        return self._username


def init_login(app):
    oauth = OAuth(app)
    oauth.init_app(app)
    oauth.register(
        name='provider',
        client_id=app.config['CIRUN_OAUTH_CLIENT_ID'],
        client_secret=app.config['CIRUN_OAUTH_CLIENT_SECRET'],
        access_token_url=app.config['CIRUN_ACCESS_TOKEN_URL'],
        authorize_url=app.config['CIRUN_AUTHORIZE_URL'],
        api_base_url=app.config['CIRUN_API_BASE_URL'],
        # client_kwargs={'scope': 'read:user'},
    )

    @app.route('/login')
    def login():
        log.debug('Redirecting to OAuth provider')
        redirect_uri = url_for('auth', _external=True).replace('http', 'https')
        return oauth.provider.authorize_redirect(redirect_uri)

    @app.route('/auth')
    def auth():
        log.debug('Received authentication request')
        try:
            oauth.provider.authorize_access_token()
        except AuthlibBaseError:
            raise abort(401, 'OAuth validation failed. Please retry.')

        profile = oauth.provider.get('User').json()
        user = User(profile['username'])
        log.info('Login attempt for %s', user)

        groups = oauth.provider.get('G2').json()['groups']
        if app.config['CIRUN_AUTHORIZED_GROUP'] not in groups:
            log.info('%s is not in a valid group', user)
            raise abort(403, '%s is not in a valid group' % user)

        login_user(user, remember=False, duration=timedelta(seconds=60))
        log.info('User %s logged in successfully', user)
        return redirect('/')

    @app.route('/logout')
    def logout():
        logout_user()
        return 'Logged out successfully'

    login_manager = LoginManager()
    login_manager.login_view
    login_manager.init_app(app)
    login_manager.login_view = 'login'

    @login_manager.user_loader
    def load_user(user_id):
        return User(user_id)
