from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import json
import os

import gitlab_runner_api
import pkg_resources
import six

from .logging import log

RUNNER_DATA_FN = os.environ['CIRUN_RUNNER_DATA']


class Runner(object):
    @classmethod
    def read_all(cls):
        if os.path.isfile(RUNNER_DATA_FN):
            with open(RUNNER_DATA_FN, 'rt') as fp:
                all_data = json.load(fp)
        else:
            all_data = {}
        return {k: cls(k, *v) for k, v in all_data.items()}

    def __init__(self, id, gitlab_runner, name, url, tags, plugin_name, active):
        self.id = id
        if isinstance(gitlab_runner, six.string_types):
            gitlab_runner = gitlab_runner_api.Runner.loads(gitlab_runner)
        self.gitlab_runner = gitlab_runner
        self.name = name
        self.url = url
        self.tags = tags
        self.plugin_name = plugin_name
        self.active = active

    @property
    def plugin(self):
        return available_plugins[self.plugin_name]

    def write(self):
        # FIXME: This isn't process safe but it probably doesn't matter
        if os.path.isfile(RUNNER_DATA_FN):
            with open(RUNNER_DATA_FN, 'rt') as fp:
                all_data = json.load(fp)
        else:
            all_data = {}

        all_data[self.id] = (
            self.gitlab_runner.dumps(),
            self.name,
            self.url,
            self.tags,
            self.plugin_name,
            self.active,
        )

        with open(RUNNER_DATA_FN, 'wt') as fp:
            json.dump(all_data, fp)
        log.debug('Added runner to database')

        # TODO: Register in beat


available_plugins = {
    '%s:%s' % (entry_point.module_name, entry_point.name): entry_point.load()
    for entry_point in pkg_resources.iter_entry_points('cirun_plugins')
}
