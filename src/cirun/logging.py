from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__all__ = [
    'log',
]

import logging

import colorlog

VERBOSE = 5
logging.addLevelName(VERBOSE, 'VERBOSE')
ALWAYS = 1000
logging.addLevelName(ALWAYS, 'ALWAYS')


def verbose(msg, *args, **kwargs):
    if log.isEnabledFor(VERBOSE):
        log._log(VERBOSE, msg, args, **kwargs)


def always(msg, *args, **kwargs):
    if log.isEnabledFor(ALWAYS):
        log._log(ALWAYS, msg, args, **kwargs)


class CallbackHandler(logging.StreamHandler):
    def __init__(self):
        super(CallbackHandler, self).__init__()
        self.callback = None

    def emit(self, record):
        super(CallbackHandler, self).emit(record)
        if self.callback is not None:
            msg = self.format(record)
            self.callback(record, msg)


handler = CallbackHandler()
handler.setFormatter(colorlog.ColoredFormatter(
    '%(log_color)s%(levelname)s:%(name)s:%(reset)s%(message)s',
    datefmt=True,
    reset=True,
    log_colors={
        'ALWAYS': 'blue',
        'FATAL': 'purple',
        'ERROR': 'red',
        'WARN': 'yellow',
        'WARNING': 'yellow',
        'INFO': 'green',
        'DEBUG': 'white',
        'VERBOSE': 'cyan',
    },
    secondary_log_colors={},
    style='%',
))

log = logging.getLogger('cirun')
log.addHandler(handler)
log.setLevel('VERBOSE')

# Add the extra log methods
log.verbose = verbose
log.always = always
