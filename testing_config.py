LOGIN_DISABLED = True
SECRET_KEY = b'This should be a long random secret'

CIRUN_AUTHORIZED_GROUP = 'cirun-authorised-egroup'

CIRUN_OAUTH_CLIENT_ID = 'Client ID for OAuth 2 provider'
CIRUN_OAUTH_CLIENT_SECRET = 'Client Secret for OAuth 2 provider'
CIRUN_ACCESS_TOKEN_URL = 'https://oauth.web.cern.ch/OAuth/Token'
CIRUN_AUTHORIZE_URL = 'https://oauth.web.cern.ch/OAuth/Authorize'
CIRUN_API_BASE_URL = 'https://oauthresource.web.cern.ch/api/'
