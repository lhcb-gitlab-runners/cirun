from setuptools import setup, find_packages
import os
from os.path import abspath, dirname, join
from io import open

here = abspath(dirname(__file__))

# Get the long description from the README file
with open(join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


package_data = []
for root, dirs, files in os.walk(join(here, 'src/cirun/data')):
    package_data += [join(root, fn) for fn in files]


setup(
    name='cirun',
    use_scm_version=True,
    description='Create customised GitLab CI runners',
    long_description=long_description,
    long_description_content_type='text/markdown',

    url='https://gitlab.cern.ch/lhcb-gitlab-runners/cirun',

    author='LHCb',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    keywords='LHCb CERN GitLab CI testing openshift',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    python_requires='>=2.7, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, !=3.4.*, !=3.5.*',
    setup_requires=['setuptools_scm'],
    install_requires=[
        'authlib',
        'celery',
        'colorlog',
        'flask',
        'flask_login',
        'gitlab_runner_api',
        'jinja2',
        'pika',
        'setuptools',
        'six',
    ],
    package_data={
        'cirun': package_data,
    },
    zip_safe=False,
    project_urls={
        'Bug Reports': 'https://gitlab.cern.ch/lhcb-gitlab-runners/cirun/issues',
        'Source': 'https://gitlab.cern.ch/lhcb-gitlab-runners/cirun',
    },
)
